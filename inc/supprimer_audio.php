<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$id = $_POST['id'];
	$fichier = $_POST['fichier'];
	$question = $_POST['question'];
	$admin = false;
	if (isset($_SESSION['digirecord'][$id]['reponse'])) {
		$reponse = $_SESSION['digirecord'][$id]['reponse'];
		$admin = true;
	} else {
		$reponse = strtolower($_POST['reponse']);
	}
	$stmt = $db->prepare('SELECT * FROM digirecord_enregistrements WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else {
			$questionSecrete = $resultat[0]['question'];
			if ($admin === true) {
				$question = $questionSecrete;
			}
			$reponseSecrete = $resultat[0]['reponse'];
			if (($admin === true && $reponse === $reponseSecrete) || ($question === $questionSecrete && password_verify($reponse, $reponseSecrete))) {
				$stmt = $db->prepare('DELETE FROM digirecord_enregistrements WHERE url = :url');
				if ($stmt->execute(array('url' => $id))) {
					if (file_exists('../fichiers/' . $fichier)) {
						unlink('../fichiers/' . $fichier);
					}
					echo 'enregistrement_supprime';
				} else {
					echo 'erreur';
				}
			} else {
				echo 'non_autorise';
			}
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
