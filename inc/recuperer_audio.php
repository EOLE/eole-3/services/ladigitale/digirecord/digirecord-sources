<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digirecord'][$id]['reponse'])) {
		$reponse = $_SESSION['digirecord'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digirecord_enregistrements WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($donnees = $stmt->fetchAll()) {
			$admin = false;
			if (count($donnees, COUNT_NORMAL) > 0 && $donnees[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$digidrive = 0;
			if (isset($_SESSION['digirecord'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digirecord'][$id]['digidrive'];
			}
			echo json_encode(array('nom' => $donnees[0]['nom'], 'fichier' => $donnees[0]['fichier'], 'date' =>  $donnees[0]['date'], 'admin' =>  $admin, 'digidrive' => $digidrive));
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
